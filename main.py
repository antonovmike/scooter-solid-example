from scooter import Scooter, Client, Employee, ScooterStatus, ScooterStatusChecker

# Example Usage
scooter = Scooter(ScooterStatus.AVAILABLE)
print(scooter.is_available())

client = Client()
employee = Employee()
status_checker = ScooterStatusChecker()

# Client rents a scooter
client.rent_scooter(scooter, status_checker)

# Employee services a scooter
employee.service_scooter(scooter, status_checker)

scooter.change_status(ScooterStatus.AVAILABLE)
print(scooter.is_available())
