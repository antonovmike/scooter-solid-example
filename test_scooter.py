import unittest
from unittest.mock import patch
from datetime import datetime
from scooter import (
    Scooter,
    ScooterStatus,
    Client,
    Employee,
    ScooterStatusChecker,
    InvalidScooterStatusError,
)


class TestScooter(unittest.TestCase):
    def setUp(self):
        self.scooter = Scooter(ScooterStatus.AVAILABLE)

    def test_change_status(self):
        self.scooter.change_status(ScooterStatus.RENTED)
        self.assertEqual(self.scooter.status, ScooterStatus.RENTED)

    def test_is_available(self):
        self.assertTrue(self.scooter.is_available())

    def test_invalid_status_raises_exception(self):
        with self.assertRaises(InvalidScooterStatusError):
            self.scooter.change_status("invalid_status")


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.scooter = Scooter(ScooterStatus.AVAILABLE)
        self.status_checker = ScooterStatusChecker()

    def test_rent_scooter(self):
        self.client.rent_scooter(self.scooter, self.status_checker)
        self.assertEqual(self.scooter.status, ScooterStatus.RENTED)


class TestEmployee(unittest.TestCase):
    def setUp(self):
        self.employee = Employee()
        self.scooter = Scooter(ScooterStatus.RENTED)
        self.status_checker = ScooterStatusChecker()

    def test_service_scooter(self):
        self.employee.service_scooter(self.scooter, self.status_checker)
        self.assertEqual(self.scooter.status, ScooterStatus.SERVICE)
